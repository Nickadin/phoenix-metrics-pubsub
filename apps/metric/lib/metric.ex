defmodule Metric do
  @moduledoc """
  Documentation for Metric.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Metric.hello
      :world

  """
  def hello do
    :world
  end
end
