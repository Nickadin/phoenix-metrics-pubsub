defmodule Metric.Publisher do
  use GenStage
  alias GenStage.BroadcastDispatcher

  defmodule Message do
    defstruct body: %{}, topic: nil
  end

  def start_link do
    GenStage.start_link(__MODULE__, 1..10 |> Enum.map(&Integer.to_string/1), name: __MODULE__)
  end

  def init(instance_ids) do
    #add self message to publish for each id randomly
    for id <- instance_ids, do: send(self, {:metrics, id})

    {:producer, {:queue.new, 0}, dispatcher: BroadcastDispatcher}
  end

  def handle_info({:metrics, id}, state) do
    Process.send_after(self, {:metrics, id}, :random.uniform(2000) + 3000)

    metrics = %{
      id: id,
      cpu: :rand.uniform(100),
      memory: :rand.uniform(50) + 50,
      disk_space: :rand.uniform(5) + 70
    }
    #temp workaround for this example, since we also have generation logic here
    spawn(fn -> GenStage.call(__MODULE__, {:publish, metrics}) end)

    {:noreply, [], state}
  end

  #Add event
  def handle_call({:publish, %{id: id} = body}, from, {queue, demand}) do
    message = %Message{body: body, topic: id}

    dispatch_events(:queue.in({from, message}, queue), demand, [])
  end

  def handle_demand(incoming_demand, {queue, demand}) do
    dispatch_events(queue, incoming_demand + demand, [])
  end

  defp dispatch_events(queue, demand, events) do
    with d when d > 0 <- demand,
        {item, queue} = :queue.out(queue),
        {:value, {from, event}} <- item do
      GenStage.reply(from, :ok)
      dispatch_events(queue, demand - 1, [event | events])
    else
      _ -> {:noreply, Enum.reverse(events), {queue, demand}}
    end
  end
end