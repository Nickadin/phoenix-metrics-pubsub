defmodule SocketApi.Supervisors.MetricSubscriber do
  @moduledoc false
  use Supervisor

  @name SocketApi.Supervisors.MetricSubscriber
  @metric_subscriber SocketApi.Subscribers.Metric

  def start_link do
    Supervisor.start_link(__MODULE__, [], name: @name)
  end

  def start_child(callback_process, topics) do
    Supervisor.start_child(@name, [callback_process, topics])
  end

  def stop_child(pid) do
    Supervisor.terminate_child(@name, pid)
  end

  def init([]) do
    children = [
      worker(@metric_subscriber, [], restart: :temporary)
    ]

    supervise(children, strategy: :simple_one_for_one)
  end
end