defmodule SocketApi.Web.MetricChannel do
  use Phoenix.Channel
  require Logger

  def join("metrics:" <> ids, _message, socket) do
    #no auth required this example
    id_list = String.split(ids, ",")

    {:ok, subscriber} = SocketApi.Supervisors.MetricSubscriber.start_child(self, id_list)
    #We use this to properly clean up the process when leaving the channel
    Logger.debug("Register subscriber #{inspect subscriber}")
    socket = assign(socket, :subscriber, subscriber)

    {:ok, socket}
  end

  def handle_info({:metrics, %{body: %{id: id} = metric}}, socket) do
    push(socket, "live_metric:#{id}", metric)
    {:noreply, socket}
  end

  def terminate(reason, socket) do
    subscriber = socket.assigns.subscriber

    #Remove the lingering subscriber
    Logger.debug("Cleaning up subscriber #{inspect subscriber}")
    :ok = SocketApi.Supervisors.MetricSubscriber.stop_child(subscriber)

    :ok
  end
end