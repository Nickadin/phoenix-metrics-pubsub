defmodule SocketApi.Subscribers.Metric do
  use GenStage

  def start_link(socket, topics) do
    GenStage.start_link(__MODULE__, {socket, topics})
  end

  def init({socket, topics}) do
    producer = [
      {Metric.Publisher, selector: fn %{topic: topic} -> Enum.member?(topics, topic) end}
    ]

    {:consumer,socket, subscribe_to: producer}
  end

  def handle_events(events, _from, socket) do
    for event <- events do
      send(socket, {:metrics, event})
    end

    {:noreply, [], socket}
  end
end