import React from 'react'

import CircularProgressbar from 'react-circular-progressbar';

export default class HardDisk extends React.Component {
  constructor(props) {
    const {socket, id} = props
    super(props)

    this.state = {percentage: 0}

    socket.on(`live_metric:${id}`, ({disk_space}) => {
      this.setState({percentage: disk_space})
    })
  }

  render() {
    return (
      <CircularProgressbar percentage={this.state.percentage} classForPercentage={() => "metric-hdd"} />
    )
  }
}