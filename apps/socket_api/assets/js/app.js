import "phoenix_html"

import getSocket from "./socket"

import React from 'react'
import { render } from 'react-dom'

import Cpu from './cpu'
import Memory from './memory'
import HardDisk from './harddisk'

const INSTANCES = ["1", "2", "3", "4", "5", "6", "7"]
const socket = getSocket(INSTANCES)

const renderedInstances = INSTANCES.map(instanceId => {
  return (
    <div className="ci-row">
      <h2>Instance {instanceId}</h2>
      <Cpu className="metric" socket={socket} id={instanceId} />
      <Memory className="metric" socket={socket} id={instanceId} />
      <HardDisk className="metric" socket={socket} id={instanceId} />
    </div>
  )
});

render(<div>{renderedInstances}</div>, document.getElementById('root'))