import React from 'react'

import CircularProgressbar from 'react-circular-progressbar';

export default class Memory extends React.Component {
  constructor(props) {
    const {socket, id} = props
    super(props)

    this.state = {percentage: 0}

    socket.on(`live_metric:${id}`, ({memory}) => {
      this.setState({percentage: memory})
    })
  }

  render() {
    return (
      <CircularProgressbar percentage={this.state.percentage} classForPercentage={() => "metric-memory"} />
    )
  }
}