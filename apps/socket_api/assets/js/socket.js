import {Socket} from "phoenix"

export default (instanceIds) => {
  let socket = new Socket("/socket", {params: {token: window.userToken}})

  socket.connect()

  let channel = socket.channel(`metrics:${instanceIds.join(',')}`, {})
  channel.join()
    .receive("ok", resp => { console.log("Joined successfully", resp) })
    .receive("error", resp => { console.log("Unable to join", resp) })
  return channel
}